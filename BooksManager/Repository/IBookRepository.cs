﻿using BooksManager.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksManager.Repository
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetBooks();
        Book GetBookByID(int id);
        void AddNewBook(Book book);
        void DeleteBook(Book book);
        void UpdateBook(Book book);
    }
}
