﻿using BooksManager.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksManager.Repository
{
    public class BooksRepository : IBookRepository
    {
     public IEnumerable<Book> GetBooks() => BookManagement.Instance.GetBookList();
        public Book GetBookByID(int bookId) => BookManagement.Instance.GetBookByID(bookId);
        public void AddNewBook(Book book) => BookManagement.Instance.AddNewBook(book);
        public void UpdateBook(Book book) => BookManagement.Instance.UpdateBook(book);
        public void DeleteBook(Book book) => BookManagement.Instance.DeleteBook(book);
    }
}
