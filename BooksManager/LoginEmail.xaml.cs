﻿using BooksManager.Repository;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BooksManager
{
    /// <summary>
    /// Interaction logic for LoginEmail.xaml
    /// </summary>
    public partial class LoginEmail : Window
    {
        public LoginEmail()
        {
            InitializeComponent();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(passwordBox.Password) && passwordBox.Password.Length > 0)
                textPassword.Visibility = Visibility.Collapsed;
            else
                textPassword.Visibility = Visibility.Visible;
        }

        private void textPassword_MouseDown(object sender, MouseButtonEventArgs e)
        {
            passwordBox.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string email = txtEmail.Text;
            string password = passwordBox.Password;

            // Call the CheckLogin method to verify the login credentials against the database
            bool loginSuccess = CheckLogin(email, password);
            if (loginSuccess)
            {
                // Login successful
                MessageBox.Show("Login successful!");
                // Perform further operations after successful login

                // Open the Management form
                Management managementForm = new Management();
                managementForm.Show();

                // Close the current form
                this.Close();
            }
            else
            {
                // Login failed
                MessageBox.Show("Login failed. Please check your email and password.");
            }
        }

        private bool CheckLogin(string email, string password)
        {
            bool loginSuccess = false;

            string connectionString = "Server=DESKTOP-ETC4R5E\\CHICUONG;database=BookManagementDB;uid=sa;pwd=123456;";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "SELECT * FROM Users WHERE Email = @Email AND Password = @Password";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Email", email);
                command.Parameters.AddWithValue("@Password", password);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        // User exists and login successful
                        loginSuccess = true;
                    }

                    reader.Close();
                }
                catch (Exception ex)
                {
                    // Handle exception if any
                    Console.WriteLine(ex.Message);
                }
            }

            return loginSuccess;
        }

        private void txtEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEmail.Text) && txtEmail.Text.Length > 0)
                textEmail.Visibility = Visibility.Collapsed;
            else
                textEmail.Visibility = Visibility.Visible;
        }

        private void textEmail_MouseDown(object sender, MouseButtonEventArgs e)
        {
            txtEmail.Focus();
        }
    }
}
