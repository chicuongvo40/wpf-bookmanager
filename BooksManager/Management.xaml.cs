﻿using BooksManager.DataAccess;
using BooksManager.Repository;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BooksManager
{
    /// <summary>
    /// Interaction logic for Management.xaml
    /// </summary>
    public partial class Management : Window
    {
        private readonly BookManagement bookManagement;

        public string uri_avatar = AppDomain.CurrentDomain.BaseDirectory + @"C:\Users\Admin\Desktop\New folder\BooksManager\BooksManager\Images2\no_avatar.jpg";
        public Management()
        {
            InitializeComponent();
            bookManagement = BookManagement.Instance;
        }
        private Book GetBookObject()
        {
            try
            {
                return new Book
                {
                    Title = txtTitle.Text,
                    Price = Decimal.Parse(txtPrice.Text),
                    Author = txtAuthor.Text,
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Book");
                return null;
            }
        }

        public void LoadBookList()
        {
            lvBook.ItemsSource = bookManagement.GetBookList();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string key = keySearch.Text;
            BookManagementDBContext db = new BookManagementDBContext();
            var list = db.Books.Where(x => x.Title.Contains(key)).ToList();
            lvBook.ItemsSource = list;
        }

        private void picAvatar_MouseDown(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opendialog = new OpenFileDialog();
            opendialog.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.gif";
            if (opendialog.ShowDialog() == true)
            {
                picAvatar.ImageSource = new BitmapImage(new Uri(opendialog.FileName));
                uri_avatar = opendialog.FileName;
            }
        }

        private void Button_LOAD(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadBookList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Book list");
            }
        }

        private void Button_ADD(object sender, RoutedEventArgs e)
        {
            try
            {
                Book book = GetBookObject();
                if (book != null)
                {
                    bookManagement.AddNewBook(book);
                    LoadBookList();
                    MessageBox.Show($"{book.Title} inserted successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Insert book");
            }
        }


        private void Button_Edit(object sender, RoutedEventArgs e)
        {
            try
            {
                BookManagementDBContext context = new BookManagementDBContext();
                var book = context.Books.SingleOrDefault(x => x.BookId == Convert.ToInt32(txtBookId.Text));

                if (book != null)
                {
                    book.Title = txtTitle.Text;
                    book.Price = Decimal.Parse(txtPrice.Text);
                    book.Author = txtAuthor.Text;


                    context.SaveChanges();
                    LoadBookList();
                    MessageBox.Show($"{book.Title}updated successfully");
                }
                else
                {
                    MessageBox.Show("Student not found", "Update book");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Update book");
            }
        }

        private void Button_DELETE(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var context = new BookManagementDBContext())
                {
                    var book = context.Books.SingleOrDefault(x => x.BookId == Convert.ToInt32(txtBookId.Text));
                    if (book != null)
                    {
                        context.Books.Remove(book);
                        context.SaveChanges();
                        LoadBookList();
                        MessageBox.Show($"{book.Title} deleted successfully");
                    }
                    else
                    {
                        MessageBox.Show("Book not found", "Delete book");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete book");
            }
        }
    }
}
