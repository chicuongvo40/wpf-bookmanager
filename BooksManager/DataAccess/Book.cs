﻿using System;
using System.Collections.Generic;

namespace BooksManager.DataAccess
{
    public partial class Book
    {
        public int BookId { get; set; }
        public string? Title { get; set; }
        public string? Author { get; set; }
        public decimal? Price { get; set; }
        public int? UserId { get; set; }

        public virtual User? User { get; set; }
    }
}
