﻿using System;
using System.Collections.Generic;

namespace BooksManager.DataAccess
{
    public partial class User
    {
        public User()
        {
            Books = new HashSet<Book>();
        }

        public int UserId { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
