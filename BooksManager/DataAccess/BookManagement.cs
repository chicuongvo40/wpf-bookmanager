﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;

namespace BooksManager.DataAccess
{
    public class BookManagement
    {
        private static BookManagement instance = null;
        public static readonly object instanceLock = new object();
        private BookManagement() { }
        public static BookManagement Instance
        {
            get {
                lock (instanceLock)
                {
                    if (instance is null)
                    {
                        instance = new BookManagement();
                    }
                    return instance;
                }
            }
        }
        public IEnumerable<Book> GetBookList()
        {
            List<Book> books;
            try
            {
                var bookDB = new BookManagementDBContext();
                books = bookDB.Books.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return books;
        }
        public Book GetBookByID(int id) 
        {
            Book book = null;
            try
            {
                var bookDB = new BookManagementDBContext();
                book = bookDB.Books.SingleOrDefault(x => x.BookId == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return book;
        }
        public void AddNewBook(Book book)
        {
            try
            {
                //Book _book = GetBookByID(book.BookId);
                //if (_book is null)
                //{
                    var bookDB = new BookManagementDBContext();
                    bookDB.Books.Add(book);
                    bookDB.SaveChanges();
                //}
                //else
                //{
                //    throw new Exception("The student is already exist.");
                //}
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public void UpdateBook(Book book)
        {
            try
            {
                Book _newbook = GetBookByID(book.BookId);
                if (_newbook is null)
                {
                    var bookDB = new BookManagementDBContext();
                    bookDB.Entry<Book>(book).State = EntityState.Modified;
                    bookDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The book does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteBook(Book book)
        {
            try
            {
                Book _book = GetBookByID(book.BookId);
                if (_book is null)
                {
                    var bookDB = new BookManagementDBContext();
                    bookDB.Books.Remove(book);
                    bookDB.SaveChanges();
                }
                else
                {
                    throw new Exception("The book is already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

